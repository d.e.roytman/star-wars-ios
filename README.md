## Star Wars iOS
This application was inspired by [https://swapi.dev](SWAPI). The initial idea was to build the application which source code could represent some examples of modern (or not such modern) and widely used technologies. Such as SwiftUI, Combine, CoreData, etc.

The current status is :
1. Prepared barebone for networking and parsing data from SWAPI.
2. Added some unit tests
3. Started work on main screen.
4. Design was prepared but still not implemented.

## TODO
1. Finish application's barebone
2. Add offline mode with CoreData
3. Add UI tests
4. Finilize the design
5. Setup CI/CD
