import Foundation
import UIKit

final class Serializer<T: Codable> {
  private let jsonEncoder: JSONEncoder
  
  init(jsonEncoder: JSONEncoder = JSONEncoder()) {
    self.jsonEncoder = jsonEncoder
  }
  
  func serialize(object: T) -> String {
    do {
      let data = try jsonEncoder.encode(object)
      guard let result = String(data: data, encoding: .utf8) else {
        fatalError("Data <\(data)> is expected to be converted to string")
      }
      return result
    } catch {
      fatalError("Object <\(object)> is expected to be serialized. Error: <\(error.localizedDescription)>")
    }
  }
}
