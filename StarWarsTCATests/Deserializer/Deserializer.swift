import Foundation
final class Deserializer<T: Decodable> {
  private let jsonDecoder: JSONDecoder
  
  init(jsonDecoder: JSONDecoder = JSONDecoder()) {
    self.jsonDecoder = jsonDecoder
  }
  
  func deserialize(string: String) -> T {
    do {
      guard let data = string.data(using: .utf8) else {
        fatalError("String expected to be created from String <\(string)>")
      }
      let result = try jsonDecoder.decode(T.self, from: data)
      return result
    } catch {
      print(error)
      fatalError("String <\(string)> is expected to be deserialized. Error: <\(error.localizedDescription)>")
    }
  }
}
