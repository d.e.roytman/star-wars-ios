struct Starships: Codable, Equatable {
  let count: Int
  let next: String?
  let previous: String?
  let starships: [Starship]
  
  enum CodingKeys: String, CodingKey {
    case count, next, previous
    case starships = "results"
  }
}
