struct Species: Codable, Equatable {
  let count: Int
  let next: String?
  let previous: String?
  let species: [Specie]
  
  enum CodingKeys: String, CodingKey {
    case count, next, previous
    case species = "results"
  }
}
