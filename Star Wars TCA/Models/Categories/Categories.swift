import Foundation

struct Categories: Codable, Equatable {

  enum AdditionalInfoKeys: String, CodingKey, CaseIterable {
    case people, planets, films, species, vehicles, starships
  }
  
  let categories: [Category]
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: AdditionalInfoKeys.self)
    categories = try AdditionalInfoKeys.allCases
      .map { casePath -> (String, String, Category.Kind) in
        let value = try container.decode(String.self, forKey: casePath)
        return (casePath.rawValue, value, casePath.toKind)
      }
      .compactMap { title, path, kind in
        guard let url = URL(string: path) else { return nil }
        return (title, url, kind)
      }
      .map { title, url, kind in return Category(title: title, url: url, kind: kind) }
  }

}

extension Categories.AdditionalInfoKeys {
  fileprivate var toKind: Category.Kind {
    guard let kind = Category.Kind(rawValue: rawValue) else {
      fatalError("Kind is expected to be created from <\(rawValue)>")
    }
    return kind
  }
}
