import Foundation

struct Category: Codable, Equatable {
  
  enum Kind: String, Equatable, CaseIterable {
    case people, planets, films, species, vehicles, starships
  }
  
  enum CodingKeys: String, CodingKey, CaseIterable {
    case title, url, kind
  }
  
  enum DecodingError: Error {
    case urlDecodingError(reason: String)
    case kindDecodingError(reason: String)
  }
  
  let title: String
  let url: URL
  let kind: Kind
  
  init(title: String, url: URL, kind: Kind) {
    self.title = title
    self.url = url
    self.kind = kind
  }
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    let path = try container.decode(String.self, forKey: .url)
    guard let url = URL(string: path) else {
      throw DecodingError.urlDecodingError(reason: "Can't create URL from path <\(path)>")
    }
    self.url = url
    let rawValue = try container.decode(String.self, forKey: .kind)
    guard let kind = Kind(rawValue: rawValue) else {
      throw DecodingError.urlDecodingError(reason: "Can't create Kind from rawValue <\(rawValue)>")
    }
    self.kind = kind
    title = try container.decode(String.self, forKey: .title)
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(kind.rawValue, forKey: .kind)
    try container.encode(url.absoluteString, forKey: .url)
    try container.encode(title, forKey: .title)
  }
}
