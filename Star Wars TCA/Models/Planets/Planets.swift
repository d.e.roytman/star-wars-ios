struct Planets: Codable, Equatable {
  let count: Int
  let next: String?
  let previous: String?
  let planets: [Planet]
  
  enum CodingKeys: String, CodingKey {
    case count, next, previous
    case planets = "results"
  }
}
