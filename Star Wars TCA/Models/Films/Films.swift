struct Films: Codable, Equatable {
  let count: Int
  let films: [Film]
  
  enum CodingKeys: String, CodingKey {
    case count
    case films = "results"
  }
}
