struct People: Codable, Equatable {
  let count: Int
  let next: String?
  let people: [Person]
  
  enum CodingKeys: String, CodingKey {
    case count
    case next
    case people = "results"
  }
}
