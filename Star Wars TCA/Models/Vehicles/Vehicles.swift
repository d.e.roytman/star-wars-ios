struct Vehicles: Codable, Equatable {
  let count: Int
  let next: String?
  let previous: String?
  let vehicles: [Vehicle]
  
  enum CodingKeys: String, CodingKey {
    case count, next, previous
    case vehicles = "results"
  }
}
