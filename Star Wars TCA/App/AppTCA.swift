import ComposableArchitecture

struct AppState: Equatable {
  var categories: CategoriesState
//  var people: PeopleState?
//  var films: FilmsState?
//  var starships: StarshipsState?
//  var species: SpeciesState?
//  var vehicles: VehiclesState?
//  var planets: PlanetsState?
}

enum AppAction: Equatable {
  case categories(CategoriesAction)
  case people
  case films
  case starships
  case species
  case vehicles
  case planets
}

struct AppEnvironment {
  let scheduler: AnySchedulerOf<DispatchQueue>
  let fetcherService: FetcherServiceProtocol
}

typealias AppReducer = Reducer<AppState, AppAction, AppEnvironment>
let appReducer = AppReducer.combine(
  categoriesReducer.pullback(
    state: \.categories,
    action: /AppAction.categories,
    environment: { env in
      CategoriesEnvironment(scheduler: env.scheduler, fetcherService: env.fetcherService)
    }
  ),
  Reducer  { state, action, env in
    switch action {
    case .categories(let categoriesAction):
      switch categoriesAction {
      case .category(let id, let action):
        switch action {
        case .filmsDidFetch(let results):
          print(results)
          
        case .peopleDidFetch(let results):
          print(results)
          
        case .starshipsDidFetch(let results):
          print(results)
          
        case .vehiclesDidFetch(let results):
          print(results)
          
        case .planetsDidFetch(let results):
          print(results)
          
        case .speciesDidFetch(let results):
          print(results)
          
        case .onButtonTap, .fetchingDidFail:
          // Do nothing
          break
        }
        return .none
        
      case .fetchingDidFail:
        // TODO: Handle error
        fatalError()
      case .onAppear:
        // Do nothing
        return .none
        
      case .categoriesDidFetch(_):
        // TODO: Handle
        return .none
      }
      
    case .people:
      return .none
      
    case .films:
      return .none
      
    case .starships:
      return .none
      
    case .species:
      return .none
      
    case .vehicles:
      return .none
      
    case .planets:
      return .none
    }
  }
)
