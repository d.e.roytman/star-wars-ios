import SwiftUI
import ComposableArchitecture

@main
struct Star_Wars_TCAApp: App {
  let store: Store<AppState, AppAction> = .init(
    initialState: AppState(
      categories: CategoriesState()
    ),
    reducer: appReducer,
    environment: AppEnvironment(
      scheduler: DispatchQueue.main.eraseToAnyScheduler(),
      fetcherService: FetcherService()
    )
  )
  var body: some Scene {
    WindowGroup {
      CategoriesView(
        store: store.scope(
          state: { appState in appState.categories },
          action: { categoriesAction in AppAction.categories(categoriesAction) }
        )
      )
    }
  }
}
