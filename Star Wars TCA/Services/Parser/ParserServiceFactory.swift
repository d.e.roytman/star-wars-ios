final class ParserServiceFactory {
  
  func makeCategoriesParser() -> ParserService<Categories> {
    return ParserService()
  }
  func makePeopleParser() -> ParserService<People> {
    return ParserService()
  }
  func makePersonParser() -> ParserService<Person> {
    return ParserService()
  }
  
  func makeFilmsParser() -> ParserService<Films> {
    return ParserService()
  }
  func makeFilmParser() -> ParserService<Film> {
    return ParserService()
  }
  
  func makeSpeciesParser() -> ParserService<Species> {
    return ParserService()
  }
  func makeSpecieParser() -> ParserService<Specie> {
    return ParserService()
  }
  
  func makePlanetsParser() -> ParserService<Planets> {
    return ParserService()
  }
  func makePlanetParser() -> ParserService<Planet> {
    return ParserService()
  }
  
  func makeVehiclesParser() -> ParserService<Vehicles> {
    return ParserService()
  }
  func makeVehicleParser() -> ParserService<Vehicle> {
    return ParserService()
  }
  
  func makeStarshipsParser() -> ParserService<Starships> {
    return ParserService()
  }
  func makeStarshipParser() -> ParserService<Starship> {
    return ParserService()
  }
}
