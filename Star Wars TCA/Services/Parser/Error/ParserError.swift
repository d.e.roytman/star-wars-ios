//
//  ParserError.swift
//  Star Wars TCA
//
//  Created by Dmitry Roytman on 09.10.2021.
//

import Foundation

enum ParserError: Error, Equatable {
  case general(reason: String)
}
