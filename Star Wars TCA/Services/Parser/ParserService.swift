import ComposableArchitecture
import Combine
import Foundation

protocol ParserServiceProtocol: AnyObject {
  associatedtype T: Codable
  func parse(data: Data) -> AnyPublisher<T, ParserError>
}

final class ParserService<T: Codable>: ParserServiceProtocol {
  
  // MARK: - Private properties
  
  private let decoder: JSONDecoder
  
  // MARK: - Init
  
  init(decoder: JSONDecoder = JSONDecoder()) {
    self.decoder = decoder
  }
  
  // MARK: - Internal methods
  
  func parse(data: Data) -> AnyPublisher<T, ParserError> {
    return Just(data)
      .setFailureType(to: ParserError.self)
      .decode(type: T.self, decoder: decoder)
      .mapError { ParserError.general(reason: $0.localizedDescription) }
      .eraseToAnyPublisher()
  }
  
}
