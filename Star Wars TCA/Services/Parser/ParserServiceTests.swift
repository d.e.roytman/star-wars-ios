import XCTest
import Combine
@testable import StarWarsTCA

class ParserServiceTests: XCTestCase {
  
  private var cancellables: Set<AnyCancellable>!
  override func setUpWithError() throws {
    cancellables = []
  }
  
  override func tearDownWithError() throws {
    cancellables = nil
  }
  
  func testParserServiceShouldParseFilms() throws {
    let expectation = self.expectation(description: "ParserService Films")
    var parserError: ParserError?
    let parserService = ParserService<Films>()
    let deserialized = Deserializer<Films>().deserialize(string: .filmsReference)
    var result: Films?
    
    parserService
      .parse(string: .filmsReference)
      .sink { completion in
        switch completion {
        case .failure(let error):
          parserError = error
          
        case .finished:
          let serializer = Serializer<Films>()
          print(serializer.serialize(object: result!))
          expectation.fulfill()
        }
      } receiveValue: { result = $0 }
      .store(in: &cancellables)
    waitForExpectations(timeout: 1)
    XCTAssertNil(parserError)
    XCTAssertNotNil(result)
    XCTAssertEqual(result, deserialized)
  }

  func testParserServiceShouldParsePeople() throws {
    let expectation = self.expectation(description: "ParserService People")
    var parserError: ParserError?
    let parserService = ParserService<People>()
    var result: People?
    let deserialized = Deserializer<People>().deserialize(string: .peopleReference)
    parserService
      .parse(string: .peopleReference)
      .sink { completion in
        switch completion {
        case .failure(let error):
          parserError = error
          
        case .finished:
          let serializer = Serializer<People>()
          print(serializer.serialize(object: result!))
          expectation.fulfill()
        }
      } receiveValue: { result = $0 }
      .store(in: &cancellables)
    waitForExpectations(timeout: 1)
    XCTAssertNil(parserError)
    XCTAssertNotNil(result)
    XCTAssertEqual(result, deserialized)
  }
  
  func testParserServiceShouldParseCategories() throws {
    let expectation = self.expectation(description: "ParserService Categories")
    var parserError: ParserError?
    let parserService = ParserService<Categories>()
    var result: Categories?
    
    parserService
      .parse(string: .categoriesReference)
      .sink { completion in
        switch completion {
        case .failure(let error):
          parserError = error
          
        case .finished:
          let serializer = Serializer<Categories>()
          print(serializer.serialize(object: result!))
          expectation.fulfill()
        }
      } receiveValue: { result = $0 }
      .store(in: &cancellables)
    waitForExpectations(timeout: 1)
    XCTAssertNil(parserError)
    XCTAssertNotNil(result)
  }
}

extension ParserServiceProtocol {
  func parse(string: String) -> AnyPublisher<T, ParserError> {
    let data = string.data(using: .utf8)!
    return Just(data)
      .map { parse(data: $0) }
      .flatMap { $0 }
      .eraseToAnyPublisher()
  }
}
