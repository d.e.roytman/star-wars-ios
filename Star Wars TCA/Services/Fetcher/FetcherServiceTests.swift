import XCTest
import Combine
@testable import StarWarsTCA
class FetcherServiceTests: XCTestCase {
  
  private var cancelables: Set<AnyCancellable>!
  private var fetcherService: FetcherServiceProtocol!
  
  override func setUpWithError() throws {
    cancelables = []
    fetcherService = FetcherService()
  }
  
  override func tearDownWithError() throws {
    cancelables = nil
    fetcherService = nil
  }
  
  func testFetcherServiceShouldFetchFilms() throws {
    let expectation = self.expectation(description: "FetcherService Tests Films")
    var films: Films?
    var error: FetcherError?
    let reference: Films = Deserializer().deserialize(string: .filmsReference)
    fetcherService
      .fetchFilms()
      .sink { completion in
        switch completion {
        case .finished:
          expectation.fulfill()
          
        case .failure(let fetcherError):
          error = fetcherError
        }
      } receiveValue: { result in
        films = result
      }
      .store(in: &cancelables)
    waitForExpectations(timeout: 1)
    XCTAssertNotNil(films)
    XCTAssertNil(error)
    XCTAssertEqual(films, reference)
  }
  
  func testFetcherServiceShouldFetchPeople() throws {
    let expectation = self.expectation(description: "FetcherService Tests People")
    var people: People?
    var error: FetcherError?
    let reference: People = Deserializer().deserialize(string: .peopleReference)
    fetcherService
      .fetchPeople()
      .sink { completion in
        switch completion {
        case .finished:
          expectation.fulfill()
          
        case .failure(let fetcherError):
          error = fetcherError
        }
      } receiveValue: { result in
        people = result
      }
      .store(in: &cancelables)
    waitForExpectations(timeout: 1)
    XCTAssertNotNil(people)
    XCTAssertNil(error)
    XCTAssertEqual(people, reference)
  }
  
  func testFetcherServiceShouldFetchCategories() throws {
    let expectation = self.expectation(description: "FetcherService Tests Categories")
    var result: Categories?
    var error: FetcherError?
    let reference: Categories = Deserializer().deserialize(string: .categoriesReference)
    fetcherService
      .fetchCategories()
      .sink { completion in
        switch completion {
        case .finished:
          expectation.fulfill()
          
        case .failure(let fetcherError):
          error = fetcherError
        }
      } receiveValue: { value in
        result = value
      }
      .store(in: &cancelables)
    waitForExpectations(timeout: 1)
    XCTAssertNotNil(result)
    XCTAssertNil(error)
    XCTAssertEqual(result, reference)
  }
  
}
