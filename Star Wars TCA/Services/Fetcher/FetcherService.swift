import Foundation
import Combine

enum RequestType {
  case people(page: Int)
  case person(id: Int)
}

enum FetcherError: Error {
  case general(reason: String)
  case network(NetworkError)
  case parser(ParserError)
}

protocol FetcherServiceProtocol: AnyObject {
  func fetchCategories() -> AnyPublisher<Categories, FetcherError>
  // People
  func fetchPeople(url: URL?) -> AnyPublisher<People, FetcherError>
  func fetchPeople(search: String) -> AnyPublisher<People, FetcherError>
  func fetchPerson(url: URL) -> AnyPublisher<Person, FetcherError>
  // Species
  func fetchSpecies(url: URL?) -> AnyPublisher<Species, FetcherError>
  func fetchSpecies(search: String) -> AnyPublisher<Species, FetcherError>
  func fetchSpecie(url: URL) -> AnyPublisher<Specie, FetcherError>
  // Films
  func fetchFilms(url: URL?) -> AnyPublisher<Films, FetcherError>
  func fetchFilms(search: String) -> AnyPublisher<Films, FetcherError>
  func fetchFilm(url: URL) -> AnyPublisher<Film, FetcherError>
  //   Vehicles
  func fetchVehicles(url: URL?) -> AnyPublisher<Vehicles, FetcherError>
  func fetchVehicles(search: String) -> AnyPublisher<Vehicles, FetcherError>
  func fetchVehicle(url: URL) -> AnyPublisher<Vehicle, FetcherError>
  // Planets
  func fetchPlanets(url: URL?) -> AnyPublisher<Planets, FetcherError>
  func fetchPlanets(search: String) -> AnyPublisher<Planets, FetcherError>
  func fetchPlanet(url: URL) -> AnyPublisher<Planet, FetcherError>
  // Starships
  func fetchStarships(url: URL?) -> AnyPublisher<Starships, FetcherError>
  func fetchStarships(search: String) -> AnyPublisher<Starships, FetcherError>
  func fetchStarship(url: URL) -> AnyPublisher<Starship, FetcherError>
}

extension FetcherServiceProtocol {
  func fetchPeople() -> AnyPublisher<People, FetcherError> {
    fetchPeople(url: nil)
  }
  func fetchFilms() -> AnyPublisher<Films, FetcherError> {
    fetchFilms(url: nil)
  }
  func fetchStarships() -> AnyPublisher<Starships, FetcherError> {
    fetchStarships(url: nil)
  }
  func fetchVehicles() -> AnyPublisher<Vehicles, FetcherError> {
    fetchVehicles(url: nil)
  }
  func fetchSpecies() -> AnyPublisher<Species, FetcherError> {
    fetchSpecies(url: nil)
  }
  func fetchPlanets() -> AnyPublisher<Planets, FetcherError> {
    fetchPlanets(url: nil)
  }
}

final class FetcherService: FetcherServiceProtocol {
  
  // MARK: - Private properties
  
  private let parserFactory: ParserServiceFactory
  private let network: NetworkServiceProtocol
  
  private var cancelables: Set<AnyCancellable> = []
  
  // MARK: - Init
  init(
    parserFactory: ParserServiceFactory = ParserServiceFactory(),
    network: NetworkServiceProtocol = NetworkService()
  ) {
    self.parserFactory = parserFactory
    self.network = network
  }
  
  // MARK: - Internal methods
  
  // Categories

  func fetchCategories() -> AnyPublisher<Categories, FetcherError> {
    let parser = parserFactory.makeCategoriesParser()
    return makePublisher(with: network.getListOfCategories(), and: parser)
  }

  // Films

  func fetchFilms(url: URL?) -> AnyPublisher<Films, FetcherError> {
    let parser = parserFactory.makeFilmsParser()
    if let url = url {
      return makePublisher(with: url, and: parser)
    } else {
      return makePublisher(with: network.getFilms(), and: parser)
    }
  }
  
  func fetchFilms(search: String) -> AnyPublisher<Films, FetcherError> {
    let parser = parserFactory.makeFilmsParser()
    let networkPublisher = network.getData(with: .films, search: search)
    return makePublisher(with: networkPublisher, and: parser)
  }
  
  func fetchFilm(url: URL) -> AnyPublisher<Film, FetcherError> {
    let parser = parserFactory.makeFilmParser()
    return makePublisher(with: url, and: parser)
  }
  
  // Species
  
  func fetchSpecies(url: URL?) -> AnyPublisher<Species, FetcherError> {
    let parser = parserFactory.makeSpeciesParser()
    if let url = url {
      return makePublisher(with: url, and: parser)
    } else {
      return makePublisher(with: network.getSpecies(), and: parser)
    }
  }
  
  func fetchSpecies(search: String) -> AnyPublisher<Species, FetcherError> {
      let parser = parserFactory.makeSpeciesParser()
      let networkPublisher = network.getData(with: .species, search: search)
      return makePublisher(with: networkPublisher, and: parser)
  }
  
  func fetchSpecie(url: URL) -> AnyPublisher<Specie, FetcherError> {
      let parser = parserFactory.makeSpecieParser()
      return makePublisher(with: url, and: parser)
  }
  
  // People
  
  func fetchPeople(url: URL?) -> AnyPublisher<People, FetcherError> {
    let parser = parserFactory.makePeopleParser()
    if let url = url {
      return makePublisher(with: url, and: parser)
    } else {
      return makePublisher(with: network.getPeople(), and: parser)
    }
  }
  
  func fetchPeople(search: String) -> AnyPublisher<People, FetcherError> {
    let parser = parserFactory.makePeopleParser()
    let networkPublisher = network.getData(with: .people, search: search)
    return makePublisher(with: networkPublisher, and: parser)
  }

  func fetchPerson(url: URL) -> AnyPublisher<Person, FetcherError> {
    let parser = parserFactory.makePersonParser()
    return makePublisher(with: url, and: parser)
  }
  
  // Vehicles
  
  func fetchVehicles(url: URL?) -> AnyPublisher<Vehicles, FetcherError> {
    let parser = parserFactory.makeVehiclesParser()
    if let url = url {
      return makePublisher(with: url, and: parser)
    } else {
      return makePublisher(with: network.getVehicles(), and: parser)
    }
  }
  
  func fetchVehicles(search: String) -> AnyPublisher<Vehicles, FetcherError> {
      let parser = parserFactory.makeVehiclesParser()
      let networkPublisher = network.getData(with: .vehicles, search: search)
      return makePublisher(with: networkPublisher, and: parser)
  }
  
  func fetchVehicle(url: URL) -> AnyPublisher<Vehicle, FetcherError> {
      let parser = parserFactory.makeVehicleParser()
      return makePublisher(with: url, and: parser)
  }
  
  // Planets
  
  func fetchPlanets(url: URL?) -> AnyPublisher<Planets, FetcherError> {
    let parser = parserFactory.makePlanetsParser()
    if let url = url {
      return makePublisher(with: url, and: parser)
    } else {
      return makePublisher(with: network.getPlanets(), and: parser)
    }
  }
  
  func fetchPlanets(search: String) -> AnyPublisher<Planets, FetcherError> {
    let parser = parserFactory.makePlanetsParser()
    let networkPublisher = network.getData(with: .planets, search: search)
    return makePublisher(with: networkPublisher, and: parser)
  }
  
  func fetchPlanet(url: URL) -> AnyPublisher<Planet, FetcherError> {
    let parser = parserFactory.makePlanetParser()
    return makePublisher(with: url, and: parser)
  }
  // Starships
  
  func fetchStarships(url: URL?) -> AnyPublisher<Starships, FetcherError> {
    let parser = parserFactory.makeStarshipsParser()
    if let url = url {
      return makePublisher(with: url, and: parser)
    } else {
      return makePublisher(with: network.getStarships(), and: parser)
    }
  }
  
  func fetchStarships(search: String) -> AnyPublisher<Starships, FetcherError> {
    let parser = parserFactory.makeStarshipsParser()
    let networkPublisher = network.getData(with: .starships, search: search)
    return makePublisher(with: networkPublisher, and: parser)
  }
  
  func fetchStarship(url: URL) -> AnyPublisher<Starship, FetcherError> {
    let parser = parserFactory.makeStarshipParser()
    return makePublisher(with: url, and: parser)
  }
  
  // MARK: - Private methods
  
  private func makePublisher<T: Codable>(
    with publisher: AnyPublisher<Data, NetworkError>,
    and parser: ParserService<T>
  ) -> AnyPublisher<T, FetcherError> {
    return publisher
      .mapError(FetcherError.network)
      .flatMap { data in
        return parser
          .parse(data: data)
          .mapError(FetcherError.parser)
          .eraseToAnyPublisher()
      }
      .eraseToAnyPublisher()
  }
  
  private func makePublisher<T: Codable>(
    with url: URL,
    and parser: ParserService<T>
  ) -> AnyPublisher<T, FetcherError> {
    let publisher = network.getData(with: url)
    return makePublisher(with: publisher, and: parser)
  }
  
}
