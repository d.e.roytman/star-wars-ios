import Foundation

extension URL {
  var urlComponents: URLComponents {
    guard let components = URLComponents(url: self, resolvingAgainstBaseURL: false) else {
      fatalError("URLComponents is expected to be created from \(absoluteURL)")
    }
    return components
  }
  
  func added(urlQueryItems: [URLQueryItem]) -> URL {
    let componentsWithItems = urlComponents.added(queryItems: urlQueryItems)
    guard let urlWithItems = componentsWithItems.url else {
      fatalError()
    }
    return urlWithItems
  }
  
  func addedQuery(key: String, value: String) -> URL {
    let item = URLQueryItem(name: key, value: value)
    return added(urlQueryItems: [item])
  }
}
