import Foundation

extension URLQueryItem {
  static func makePage(_ page: Int) -> URLQueryItem {
    return .init(name: .page, value: String(page))
  }
  static func makeSearch(_ value: String) -> URLQueryItem {
    return .init(name: .search, value: value)
  }
}

extension String {
  fileprivate static var page: String { return "page" }
  fileprivate static var search: String { return "search" }
}
