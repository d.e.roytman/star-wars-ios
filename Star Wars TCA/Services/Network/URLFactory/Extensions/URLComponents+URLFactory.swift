import Foundation

extension URLComponents {
  mutating func add(queryItems: [URLQueryItem]) {
    if let items = self.queryItems {
      self.queryItems = items + queryItems
    } else {
      self.queryItems = queryItems
    }
  }
  func added(queryItems: [URLQueryItem]) -> URLComponents {
    var copy = self
    if let items = copy.queryItems {
      copy.queryItems = items + queryItems
    } else {
      copy.queryItems = queryItems
    }
    return copy
  }
}
