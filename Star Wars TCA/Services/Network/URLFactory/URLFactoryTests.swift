//
//  URLFactoryTests.swift
//  Star Wars TCA
//
//  Created by Dmitry Roytman on 09.10.2021.
//

import XCTest
@testable import StarWarsTCA

class URLFactoryTests: XCTestCase {

  private var factory: URLFactoryProtocol!
  private var page: Int!
  private var id: Int!
  private var searchString: String!
  private var base: URL!
  override func setUp() {
    factory = URLFactory(base: .base)
    page = 1
    id = 1
    base = URL(string: .baseURL)
  }
  
  override func tearDown() {
    factory = nil
    page = nil
    id = nil
    searchString = nil
    base = nil
  }

  // People
  
  func testURLFactoryShouldReturnPeopleURL() throws {
    let reference: URL = .peopleURL
    let result = factory.makeURL(for: .people(type: .list(page: nil)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
    print(reference.absoluteURL)
  }
  
  func testURLFactoryShouldReturnPeopleURLWithPage() throws {
    let reference: URL = .makePeopleURL(with: page)
    let result = factory.makeURL(for: .people(type: .list(page: page)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  func testURLFactoryShouldReturnPeopleURLWithId() throws {
    let reference: URL = .makePersonURL(with: id)
    let result = factory.makeURL(for: .people(type: .id(id)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  func testURLFactoryShouldReturnPeopleURLWithSearch() throws {
    searchString = "r2"
    let reference: URL = .makePeopleURL(searchString: searchString)
    let result = factory.makeURL(for: .people(type: .search(searchString)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }

  // Species
  
  func testURLFactoryShouldReturnSpeciesURL() throws {
    let reference: URL = .speciesURL
    let result = factory.makeURL(for: .species(type: .list(page: nil)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
    print(reference.absoluteURL)
  }
  
  func testURLFactoryShouldReturnSpeciesURLWithPage() throws {
    let reference: URL = .makeSpeciesURL(with: page)
    let result = factory.makeURL(for: .species(type: .list(page: page)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  func testURLFactoryShouldReturnSpeciesURLWithId() throws {
    let reference: URL = .makeSpecieURL(with: id)
    let result = factory.makeURL(for: .species(type: .id(id)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  func testURLFactoryShouldReturnSpeciesURLWithSearch() throws {
    searchString = "species"
    let reference: URL = .makeSpeciesURL(searchString: searchString)
    let result = factory.makeURL(for: .species(type: .search(searchString)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  // Vehicles
  
  func testURLFactoryShouldReturnVehiclesURL() throws {
    let reference: URL = .vehiclesURL
    let result = factory.makeURL(for: .vehicles(type: .list(page: nil)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
    print(reference.absoluteURL)
  }
  
  func testURLFactoryShouldReturnVehiclesURLWithPage() throws {
    let reference: URL = .makeVehiclesURL(with: page)
    let result = factory.makeURL(for: .vehicles(type: .list(page: page)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  func testURLFactoryShouldReturnVehiclesURLWithId() throws {
    let reference: URL = .makeVehicleURL(with: id)
    let result = factory.makeURL(for: .vehicles(type: .id(id)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  func testURLFactoryShouldReturnVehiclesURLWithSearch() throws {
    searchString = "vehicles"
    let reference: URL = .makeVehiclesURL(searchString: searchString)
    let result = factory.makeURL(for: .vehicles(type: .search(searchString)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  // Planets
  
  func testURLFactoryShouldReturnPlanetsURL() throws {
    let reference: URL = .planetsURL
    let result = factory.makeURL(for: .planets(type: .list(page: nil)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
    print(reference.absoluteURL)
  }
  
  func testURLFactoryShouldReturnPlanetsURLWithPage() throws {
    let reference: URL = .makePlanetsURL(with: page)
    let result = factory.makeURL(for: .planets(type: .list(page: page)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  func testURLFactoryShouldReturnPlanetsURLWithId() throws {
    let reference: URL = .makePlanetURL(with: id)
    let result = factory.makeURL(for: .planets(type: .id(id)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  func testURLFactoryShouldReturnPlanetsURLWithSearch() throws {
    searchString = "planets"
    let reference: URL = .makePlanetsURL(searchString: searchString)
    let result = factory.makeURL(for: .planets(type: .search(searchString)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  // Starships
  
  func testURLFactoryShouldReturnStarshipsURL() throws {
    let reference: URL = .starshipsURL
    let result = factory.makeURL(for: .starships(type: .list(page: nil)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
    print(reference.absoluteURL)
  }
  
  func testURLFactoryShouldReturnStarshipsURLWithPage() throws {
    let reference: URL = .makeStarshipsURL(with: page)
    let result = factory.makeURL(for: .starships(type: .list(page: page)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  func testURLFactoryShouldReturnStarshipsURLWithId() throws {
    let reference: URL = .makeStarshipURL(with: id)
    let result = factory.makeURL(for: .starships(type: .id(id)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  func testURLFactoryShouldReturnStarshipsURLWithSearch() throws {
    searchString = "starships"
    let reference: URL = .makeStarshipsURL(searchString: searchString)
    let result = factory.makeURL(for: .starships(type: .search(searchString)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  // Films
  
  func testURLFactoryShouldReturnFilmsURL() throws {
    let reference: URL = .filmsURL
    let result = factory.makeURL(for: .films(type: .list(page: nil)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
    print(reference.absoluteURL)
  }
  
  func testURLFactoryShouldReturnFilmsURLWithPage() throws {
    let reference: URL = .makeFilmsURL(with: page)
    let result = factory.makeURL(for: .films(type: .list(page: page)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  func testURLFactoryShouldReturnFilmsURLWithId() throws {
    let reference: URL = .makeFilmURL(with: id)
    let result = factory.makeURL(for: .films(type: .id(id)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
  
  func testURLFactoryShouldReturnFilmsURLWithSearch() throws {
    searchString = "films"
    let reference: URL = .makeFilmsURL(searchString: searchString)
    let result = factory.makeURL(for: .films(type: .search(searchString)))
    XCTAssertEqual(reference, result)
    XCTAssertEqual(reference.absoluteURL, result.absoluteURL)
  }
}

extension URL {
  static var peopleURL: URL { return URL(string: "https://swapi.dev/api/people")! }
  static var speciesURL: URL { return URL(string: "https://swapi.dev/api/species")! }
  static var vehiclesURL: URL { return URL(string: "https://swapi.dev/api/vehicles")! }
  static var planetsURL: URL { return URL(string: "https://swapi.dev/api/planets")! }
  static var starshipsURL: URL { return URL(string: "https://swapi.dev/api/starships")! }
  static var filmsURL: URL { return URL(string: "https://swapi.dev/api/films")! }
  
  static func makePeopleURL(with page: Int) -> URL { return peopleURL.added(urlQueryItems: [.makePage(page)]) }
  static func makePeopleURL(searchString: String) -> URL { return peopleURL.added(urlQueryItems: [.makeSearch(searchString)]) }
  static func makePersonURL(with id: Int) -> URL { return peopleURL.appendingPathComponent(String(id)) }
  
  static func makeSpeciesURL(with page: Int) -> URL { return speciesURL.added(urlQueryItems: [.makePage(page)]) }
  static func makeSpeciesURL(searchString: String) -> URL { return speciesURL.added(urlQueryItems: [.makeSearch(searchString)]) }
  static func makeSpecieURL(with id: Int) -> URL { return speciesURL.appendingPathComponent(String(id)) }
  
  static func makeVehiclesURL(with page: Int) -> URL { return vehiclesURL.added(urlQueryItems: [.makePage(page)]) }
  static func makeVehiclesURL(searchString: String) -> URL { return vehiclesURL.added(urlQueryItems: [.makeSearch(searchString)]) }
  static func makeVehicleURL(with id: Int) -> URL { return vehiclesURL.appendingPathComponent(String(id)) }
  
  static func makeFilmsURL(with page: Int) -> URL { return filmsURL.added(urlQueryItems: [.makePage(page)]) }
  static func makeFilmsURL(searchString: String) -> URL { return filmsURL.added(urlQueryItems: [.makeSearch(searchString)]) }
  static func makeFilmURL(with id: Int) -> URL { return filmsURL.appendingPathComponent(String(id)) }
  
  static func makePlanetsURL(with page: Int) -> URL { return planetsURL.added(urlQueryItems: [.makePage(page)]) }
  static func makePlanetsURL(searchString: String) -> URL { return planetsURL.added(urlQueryItems: [.makeSearch(searchString)]) }
  static func makePlanetURL(with id: Int) -> URL { return planetsURL.appendingPathComponent(String(id)) }
  
  static func makeStarshipsURL(with page: Int) -> URL { return starshipsURL.added(urlQueryItems: [.makePage(page)]) }
  static func makeStarshipsURL(searchString: String) -> URL { return starshipsURL.added(urlQueryItems: [.makeSearch(searchString)]) }
  static func makeStarshipURL(with id: Int) -> URL { return starshipsURL.appendingPathComponent(String(id)) }
}
