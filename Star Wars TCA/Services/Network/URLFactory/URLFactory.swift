//
//  URLFactory.swift
//  Star Wars TCA
//
//  Created by Dmitry Roytman on 07.10.2021.
//

import Foundation
import UIKit

enum Route {
  
  enum RouteType {
    case search(String)
    case id(Int)
    case list(page: Int?)
  }
  
  case films(type: RouteType)
  case planets(type: RouteType)
  case people(type: RouteType)
  case species(type: RouteType)
  case vehicles(type: RouteType)
  case starships(type: RouteType)
}

protocol URLFactoryProtocol: AnyObject {
  func makeCategoriesURL() -> URL
  func makeURL(for route: Route) -> URL
}

final class URLFactory: URLFactoryProtocol {

  // MARK: - Private properties
  
  private let base: URL
  
  // MARK: - Init
  
  init(base: URL = .base) {
    self.base = base
  }
  
  // MARK: - Methods
  
  func makeCategoriesURL() -> URL {
    return base
  }
  
  func makeURL(for route: Route) -> URL {
    return route.makeURL(with: base)
  }
  
  // MARK: - Private methods
  
  private func setPageIfNeeded(to path: URL, page: Int?) -> URL {
    guard let page = page else { return path }
    return path.added(urlQueryItems: [.makePage(page)])
  }
  
}

extension Route {
  func makeURL(with base: URL) -> URL {
    let url: URL
    switch self {
    case .films(type: let type):
      url = type.makeURL(for: base.appendingPathComponent(.filmsURLComponent))
    case .planets(type: let type):
      url = type.makeURL(for: base.appendingPathComponent(.planetsURLComponent))
    case .people(type: let type):
      url = type.makeURL(for: base .appendingPathComponent(.peopleURLComponent))
    case .species(type: let type):
      url = type.makeURL(for: base.appendingPathComponent(.speciesURLComponent))
    case .vehicles(type: let type):
      url = type.makeURL(for: base.appendingPathComponent(.vehiclesURLComponent))
    case .starships(type: let type):
      url = type.makeURL(for: base.appendingPathComponent(.starshipsURLComponent))
    }
    return url
  }
}

extension Route.RouteType {
  func makeURL(for base: URL) -> URL {
    switch self {
    case .id(let id):
      return base.appendingPathComponent(String(id))
      
    case .list(let page):
      guard let page = page else {
        return base
      }
      return base.added(urlQueryItems: [.makePage(page)])
      
    case .search(let searchString):
      return base.added(urlQueryItems: [.makeSearch(searchString)])
    }
  }
}
