import Combine
import Foundation
import UIKit

protocol NetworkServiceProtocol: AnyObject {
  typealias RequestType = NetworkService.RequestType
  
  func getListOfCategories() -> AnyPublisher<Data, NetworkError>
  
  func getData(with url: URL) -> AnyPublisher<Data, NetworkError>
  func getData(with requestType: RequestType, for page: Int?) -> AnyPublisher<Data, NetworkError>
  func getData(with requestType: RequestType, id: Int) -> AnyPublisher<Data, NetworkError>
  func getData(with requestType: RequestType, search: String) -> AnyPublisher<Data, NetworkError>
}

extension NetworkServiceProtocol {
  func getPeople() -> AnyPublisher<Data, NetworkError> {
    getData(with: .people, for: nil)
  }
  func getPlanets() -> AnyPublisher<Data, NetworkError> {
    getData(with: .planets, for: nil)
  }
  func getSpecies() -> AnyPublisher<Data, NetworkError> {
    getData(with: .species, for: nil)
  }
  func getFilms() -> AnyPublisher<Data, NetworkError> {
    getData(with: .films, for: nil)
  }
  func getVehicles() -> AnyPublisher<Data, NetworkError> {
    getData(with: .vehicles, for: nil)
  }
  func getStarships() -> AnyPublisher<Data, NetworkError> {
    getData(with: .starships, for: nil)
  }
  
  func getPeople(for page: Int?) -> AnyPublisher<Data, NetworkError> {
    getData(with: .people, for: page)
  }
  func getPlanets(for page: Int?) -> AnyPublisher<Data, NetworkError> {
    getData(with: .planets, for: page)
  }
  func getSpecies(for page: Int?) -> AnyPublisher<Data, NetworkError> {
    getData(with: .species, for: page)
  }
  func getFilms(for page: Int?) -> AnyPublisher<Data, NetworkError> {
    getData(with: .films, for: page)
  }
  func getVehicles(for page: Int?) -> AnyPublisher<Data, NetworkError> {
    getData(with: .vehicles, for: page)
  }
  func getStarships(for page: Int?) -> AnyPublisher<Data, NetworkError> {
    getData(with: .starships, for: page)
  }
  
  func getPeople(for id: Int) -> AnyPublisher<Data, NetworkError> {
    getData(with: .people, id: id)
  }
  func getPlanets(for id: Int) -> AnyPublisher<Data, NetworkError> {
    getData(with: .planets, id: id)
  }
  func getSpecies(for id: Int) -> AnyPublisher<Data, NetworkError> {
    getData(with: .species, id: id)
  }
  func getFilms(for id: Int) -> AnyPublisher<Data, NetworkError> {
    getData(with: .films, id: id)
  }
  func getVehicles(for id: Int) -> AnyPublisher<Data, NetworkError> {
    getData(with: .vehicles, id: id)
  }
  func getStarships(for id: Int) -> AnyPublisher<Data, NetworkError> {
    getData(with: .starships, id: id)
  }
}

final class NetworkService: NetworkServiceProtocol {
  
  // MARK: - Internal types
  
  enum RequestType {
    case species, people, starships, vehicles, planets, films
  }
  
  // MARK: - Private properties
  
  private let dataTaskPublisherFactory: DataTaskPublisherFactoryProtocol
  private var cancelables = Set<AnyCancellable>()
  
  // MARK: - Init
  
  init(dataTaskPublisherFactory: DataTaskPublisherFactoryProtocol = DataTaskPublisherFactory()) {
    self.dataTaskPublisherFactory = dataTaskPublisherFactory
  }
  
  func getListOfCategories() -> AnyPublisher<Data, NetworkError> {
    let publisher = dataTaskPublisherFactory.makeCategoriesPublisher()
    return makeFuture(publisher: publisher)
  }
  
  func getData(with url: URL) -> AnyPublisher<Data, NetworkError> {
    let publisher = dataTaskPublisherFactory.makePublisher(for: url)
    return makeFuture(publisher: publisher)
  }
  
  func getData(with requestType: RequestType, for page: Int?) -> AnyPublisher<Data, NetworkError> {
    let routeType: Route.RouteType = .list(page: page)
    return makePublisher(with: routeType, and: requestType)
  }
  
  func getData(with requestType: RequestType, search: String) -> AnyPublisher<Data, NetworkError> {
    let routeType: Route.RouteType = .search(search)
    return makePublisher(with: routeType, and: requestType)
  }
  
  func getData(with requestType: RequestType, id: Int) -> AnyPublisher<Data, NetworkError> {
    let routeType: Route.RouteType = .id(id)
    return makePublisher(with: routeType, and: requestType)
  }
  
  // MARK: - Private methods
  
  private func makePublisher(
    with routeType: Route.RouteType,
    and requestType: RequestType
  ) -> AnyPublisher<Data, NetworkError> {
    let route = requestType.makeRoute(with: routeType)
    let publisher = dataTaskPublisherFactory.makePublisher(for: route)
    return makeFuture(publisher: publisher)
  }
  
  private func makeFuture(publisher: URLSession.DataTaskPublisher) -> AnyPublisher<Data, NetworkError> {
    return Deferred {
      return Future { [weak self] promise in
        guard let self = self else { return }
        publisher
          .sink { completion in
            switch completion {
            case .finished:
              // Do nothing
              break
            case .failure(let error):
              promise(.failure(.networkFailure(reason: error.localizedDescription)))
            }
          } receiveValue: { data, response in
            guard let urlResponse = response as? HTTPURLResponse else {
              fatalError("HTTPURLResponse is expected to be created from <\(response.description)>")
            }
            switch urlResponse.statusCode {
            case 200:
              promise(.success(data))
            case 401:
              promise(.failure(.unauthorized))
            case 403:
              promise(.failure(.forbinden))
            case 404:
              promise(.failure(.notFound))
            case 500...599:
              promise(.failure(.internalServerError))
            default:
              promise(.failure(.general))
            }
          }
          .store(in: &self.cancelables)
      }
    }.eraseToAnyPublisher()
  }
}

extension NetworkService.RequestType {
  fileprivate func makeRoute(with type: Route.RouteType) -> Route {
    let route: Route
    switch self {
    case .species:
      route = .species(type: type)
    case .people:
      route = .people(type: type)
    case .starships:
      route = .starships(type: type)
    case .vehicles:
      route = .vehicles(type: type)
    case .planets:
      route = .planets(type: type)
    case .films:
      route = .films(type: type)
    }
    return route
  }
}
