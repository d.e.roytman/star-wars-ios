//
//  NetworkServiceTests.swift
//  StarWarsTCATests
//
//  Created by Dmitry Roytman on 09.10.2021.
//

import XCTest
import Combine
@testable import StarWarsTCA

class NetworkServiceTests: XCTestCase {
  
  private var networkService: NetworkServiceProtocol!
  private var cancellables: Set<AnyCancellable>!
  
  override func setUpWithError() throws {
    networkService = NetworkService()
    cancellables = []
  }
  
  override func tearDownWithError() throws {
    networkService = nil
    cancellables = nil
  }
  
  func testNetworkServiceShouldReceiveCategories() throws {
    let expectation = self.expectation(description: "NetworkServiceCategories")
    var networkError: NetworkError?
    var data: Data?
    var response: String?
    networkService
      .getListOfCategories()
      .sink { completion in
        switch completion {
        case .failure(let error):
          networkError = error
          
        case .finished:
          expectation.fulfill()
        }
      } receiveValue: { receivedData in
        data = receivedData
        response = String(data: receivedData, encoding: .utf8)
      }
      .store(in: &cancellables)
    waitForExpectations(timeout: 10)
    XCTAssertNil(networkError)
    XCTAssertNotNil(data)
    XCTAssertNotNil(response)
    XCTAssertEqual(response, .categoriesReference)
    XCTAssertNotEqual(response, .categoriesBrokenReference)
  }
  
  func testNetworkServiceShouldReceivePeople() throws {
    let expectation = self.expectation(description: "NetworkService People")
    var networkError: NetworkError?
    var data: Data?
    var response: String?
    networkService
      .getPeople()
      .sink { completion in
        switch completion {
        case .failure(let error):
          networkError = error
          
        case .finished:
          expectation.fulfill()
        }
      } receiveValue: { receivedData in
        data = receivedData
        response = String(data: receivedData, encoding: .utf8)
      }
      .store(in: &cancellables)
    waitForExpectations(timeout: 10)
    XCTAssertNil(networkError)
    XCTAssertNotNil(data)
    XCTAssertNotNil(response)
    XCTAssertEqual(response, .peopleReference)
    XCTAssertNotEqual(response, .peopleBrokenReference)
  }
  
  func testNetworkServiceShouldReceiveStarships() throws {
    let expectation = self.expectation(description: "NetworkService Starships")
    var networkError: NetworkError?
    var data: Data?
    var response: String?
    networkService
      .getStarships()
      .sink { completion in
        switch completion {
        case .failure(let error):
          networkError = error
          
        case .finished:
          expectation.fulfill()
        }
      } receiveValue: { receivedData in
        data = receivedData
        response = String(data: receivedData, encoding: .utf8)
      }
      .store(in: &cancellables)
    waitForExpectations(timeout: 10)
    XCTAssertNil(networkError)
    XCTAssertNotNil(data)
    XCTAssertNotNil(response)
    XCTAssertEqual(response, .starshipsReference)
    XCTAssertNotEqual(response, .starshipsBrokenReference)
  }
  
  func testNetworkServiceShouldReceiveFilms() throws {
    let expectation = self.expectation(description: "NetworkService Films")
    var networkError: NetworkError?
    var data: Data?
    var response: String?
    networkService
      .getFilms()
      .sink { completion in
        switch completion {
        case .failure(let error):
          networkError = error
          
        case .finished:
          expectation.fulfill()
        }
      } receiveValue: { receivedData in
        data = receivedData
        response = String(data: receivedData, encoding: .utf8)
      }
      .store(in: &cancellables)
    waitForExpectations(timeout: 10)
    XCTAssertNil(networkError)
    XCTAssertNotNil(data)
    XCTAssertNotNil(response)
    XCTAssertEqual(response, .filmsReference)
    XCTAssertNotEqual(response, .filmsBrokenReference)
  }
  
  func testNetworkServiceShouldReceiveVehicles() throws {
    let expectation = self.expectation(description: "NetworkService Vehicles")
    var networkError: NetworkError?
    var data: Data?
    var response: String?
    networkService
      .getVehicles()
      .sink { completion in
        switch completion {
        case .failure(let error):
          networkError = error
          
        case .finished:
          expectation.fulfill()
        }
      } receiveValue: { receivedData in
        data = receivedData
        response = String(data: receivedData, encoding: .utf8)
      }
      .store(in: &cancellables)
    waitForExpectations(timeout: 10)
    XCTAssertNil(networkError)
    XCTAssertNotNil(data)
    XCTAssertNotNil(response)
    XCTAssertEqual(response, .vehiclesReference)
    XCTAssertNotEqual(response, .vehiclesBrokenReference)
  }
  
  func testNetworkServiceShouldReceiveSpecies() throws {
    let expectation = self.expectation(description: "NetworkService Species")
    var networkError: NetworkError?
    var data: Data?
    var response: String?
    networkService
      .getSpecies()
      .sink { completion in
        switch completion {
        case .failure(let error):
          networkError = error
          
        case .finished:
          expectation.fulfill()
        }
      } receiveValue: { receivedData in
        data = receivedData
        response = String(data: receivedData, encoding: .utf8)
      }
      .store(in: &cancellables)
    waitForExpectations(timeout: 10)
    XCTAssertNil(networkError)
    XCTAssertNotNil(data)
    XCTAssertNotNil(response)
    XCTAssertEqual(response, .speciesReference)
    XCTAssertNotEqual(response, .speciesBrokenReference)
  }
  
}
