//
//  DataTaskPublisherFactory.swift
//  Star Wars TCA
//
//  Created by Dmitry Roytman on 07.10.2021.
//

import Foundation
import Combine

protocol DataTaskPublisherFactoryProtocol: AnyObject {
  func makeCategoriesPublisher() -> URLSession.DataTaskPublisher
  func makePublisher(for route: Route) -> URLSession.DataTaskPublisher
  func makePublisher(for url: URL) -> URLSession.DataTaskPublisher
}

final class DataTaskPublisherFactory: DataTaskPublisherFactoryProtocol {
  
  // MARK: - Private properties
  
  private let urlFactory: URLFactoryProtocol
  private let session: URLSession
  
  // MARK: - Init
  
  init(urlFactory: URLFactoryProtocol = URLFactory(), session: URLSession = .shared) {
    self.urlFactory = urlFactory
    self.session = session
  }
  
  // MARK: - Internal methods
  
  func makeCategoriesPublisher() -> URLSession.DataTaskPublisher {
    let url = urlFactory.makeCategoriesURL()
    let publisher = makePublisher(for: url)
    return publisher
  }
  
  func makePublisher(for route: Route) -> URLSession.DataTaskPublisher {
    let url = urlFactory.makeURL(for: route)
    let publisher = makePublisher(for: url)
    return publisher
  }
  
  func makePublisher(for url: URL) -> URLSession.DataTaskPublisher {
    return session.dataTaskPublisher(for: url)
  }
}
