enum NetworkError: Error, Equatable {
  case general
  case unauthorized
  case notFound
  case forbinden
  case internalServerError
  case networkFailure(reason: String)
}
