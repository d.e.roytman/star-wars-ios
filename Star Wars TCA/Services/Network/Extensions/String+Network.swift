extension String {
  static var baseURL: String { return "https://swapi.dev/api/" }
  static var peopleURLComponent: String { return "people" }
  static var planetsURLComponent: String { return "planets" }
  static var filmsURLComponent: String { return "films" }
  static var speciesURLComponent: String { return "species" }
  static var vehiclesURLComponent: String { return "vehicles" }
  static var starshipsURLComponent: String { return "starships" }
}
