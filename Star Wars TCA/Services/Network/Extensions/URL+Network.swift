//
//  URL+Network.swift
//  Star Wars TCA
//
//  Created by Dmitry Roytman on 07.10.2021.
//

import Foundation

extension URL {
  static var base: URL {
    guard let url = URL(string: .baseURL) else {
      fatalError("URL is expected to be created from path: <\(String.baseURL)>")
    }
    return url
  }
  static var people: URL { base.appendingPathComponent(.peopleURLComponent) }
  static var planets: URL { base.appendingPathComponent(.planetsURLComponent) }
  static var films: URL { base.appendingPathComponent(.filmsURLComponent) }
  static var species: URL { base.appendingPathComponent(.speciesURLComponent) }
  static var vehicles: URL { base.appendingPathComponent(.vehiclesURLComponent) }
  static var starships: URL { base.appendingPathComponent(.starshipsURLComponent) }
}
