import SwiftUI
import ComposableArchitecture

struct StarshipsView: View {
  let store: Store<StarshipsState, StarshipsAction>
  
  var body: some View {
    WithViewStore(store) { viewStore in
      ScrollView {
        LazyVStack {
          ForEachStore(
            store.scope(
              state: \.starships,
              action: StarshipsAction.starship(id:action:)
            ),
            content: StarshipBriefView.init(store:)
          )
          Spacer()
        }
      }
      .sheet(
        item: viewStore.binding(get: { $0.starship }, send: { _ in .starshipDidPresented }),
        onDismiss: { viewStore.send(.starshipDidPresented) },
        content: { starship in
          StarshipView(
            store: store.scope(
              state: { _ in return starship },
              action: { .starship(id: starship.id, action: $0) }
            )
          )
        }
      )
      .onAppear {
        viewStore.send(.downloadNextIfNeeded)
      }
    }
  }
}

struct StarshipsView_Previews: PreviewProvider {
  static var previews: some View {
    StarshipsView(
      store: Store(
        initialState: .init(
          starships: [],
          nextURL: "https://swapi.dev/api/starships/?page=2",
          starship: nil
        ),
        reducer: starshipsReducer,
        environment: StarshipsEnvironment(
          fetcher: FetcherService(),
          scheduler: DispatchQueue.main.eraseToAnyScheduler()
        )
      )
    )
  }
}
