import ComposableArchitecture
import Foundation

struct StarshipsState: Equatable {
  var starships: IdentifiedArrayOf<StarshipState>
  var nextURL: String?
  var starship: StarshipState?
//  var film: FilmState?
//  var person: PersonState?
}

enum StarshipsAction: Equatable {
  case downloadNext(URL)
  case starship(id: StarshipState.ID, action: StarshipAction)
  case fetched(Starships)
  case starshipDidPresented
  case fetchingDidFail
  case downloadNextIfNeeded
}

struct StarshipsEnvironment {
  let fetcher: FetcherServiceProtocol
  let scheduler: AnySchedulerOf<DispatchQueue>
}

typealias StarshipsReducer = Reducer<StarshipsState, StarshipsAction, StarshipsEnvironment>

let starshipsReducer = StarshipsReducer.combine(
  starshipReducer.forEach(
    state: \StarshipsState.starships,
    action: /StarshipsAction.starship(id:action:),
    environment: { global in .init(fetcher: global.fetcher, scheduler: global.scheduler) }
  ),
  Reducer { state, action, environment in
    switch action {
    case let .downloadNext(url):
      return environment.fetcher
        .fetchStarships(url: url)
        .map { StarshipsAction.fetched($0) }
        .replaceError(with: StarshipsAction.fetchingDidFail)
        .eraseToAnyPublisher()
        .receive(on: environment.scheduler)
        .eraseToEffect()

    case .starship(id: let id, action: let action):
      switch action {
      case .onSelect:
        guard let starship = state.starships[id: id] else {
          assertionFailure("Starship with id <\(id)> is expected to be found")
          return .none
        }
        state.starship = starship
        return .none
        
      case .openPilot, .openFilm:
        // Do nothing. Should be handled in starshipReducer
        return .none

      case .fetchedPilot(let person):
        // TODO: handle and show person screen
        assertionFailure("Implement")
        return .none
        
      case .fetchedFilm(_):
        // TODO: handle and show film screen
        assertionFailure("Implement")
        return .none
        
      case .fetchingDidFail:
        // TODO: handle and show error screen
        assertionFailure("Implement")
        return .none
      }
      
    case let .fetched(starships):
      let states = starships.starships.toStarships
      let updated = state.starships.elements + states
      state.starships = IdentifiedArrayOf(uniqueElements: updated)
      state.nextURL = starships.next
      return Effect(value: .downloadNextIfNeeded)
      
    case .fetchingDidFail:
      // TODO: handle and show error screen
      assertionFailure("Fetching was failure")
      return .none
      
    case .downloadNextIfNeeded:
      guard let next = state.nextURL else { return .none }
      guard let nextURL = URL(string: next) else {
        assertionFailure("URL is expected to be created from <\(next)>")
        return .none
      }
      
      return Effect(value: .downloadNext(nextURL))
      
    case .starshipDidPresented:
      state.starship = nil
      return .none
    }
  }
)

// MARK: - Helper

extension Array where Element == Starship {
  fileprivate var toStarships: [StarshipState] { map { $0.toStarshipState } }
}

extension Starship {
  fileprivate var toStarshipState: StarshipState {
    return .init(
      name: name,
      model: model,
      manufacturer: manufacturer,
      costInCredits: costInCredits,
      length: length,
      maxAtmospheringSpeed: maxAtmospheringSpeed,
      crew: crew,
      passengers: passengers,
      cargoCapacity: cargoCapacity,
      consumables: consumables,
      hyperdriveRating: hyperdriveRating,
      mglt: mglt,
      starshipClass: starshipClass,
      pilots: pilots.toURLs,
      films: films.toURLs,
      created: created,
      edited: edited,
      url: url.toURL!
    )
  }
}
