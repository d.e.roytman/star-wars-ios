import ComposableArchitecture
import SwiftUI

struct StarshipBriefView: View {
  let store: Store<StarshipState, StarshipAction>
  var body: some View {
    WithViewStore(store) { viewStore in
      Button {
        viewStore.send(.onSelect)
      } label: {
        HStack {
          Text(viewStore.name)
          Spacer()
          Image(systemName: "arrow.forward.circle.fill")
        }
        .padding(.horizontal, 16)
        .padding(.vertical, 8)
      }
    }
  }
}

struct StarshipBriefView_Previews: PreviewProvider {
  static var previews: some View {
    StarshipBriefView(
      store: Store(
        initialState: .preview,
        reducer: .empty,
        environment: StarshipEnvironment(
          fetcher: FetcherService(),
          scheduler: DispatchQueue.main.eraseToAnyScheduler()
        )
      )
    )
  }
}
