import ComposableArchitecture
import Foundation

struct StarshipState: Equatable, Identifiable {
  var id: String { name }
  let name, model, manufacturer, costInCredits: String
  let length, maxAtmospheringSpeed, crew, passengers: String
  let cargoCapacity, consumables, hyperdriveRating, mglt: String
  let starshipClass: String
  let pilots, films: [URL]
  let created, edited: String
  let url: URL
  
  var formattedCreatedDate: String {
    guard let date = created.toDate else { return created }
    let dateFormatter = ISO8601DateFormatter()
    dateFormatter.formatOptions = .withFullDate
    return dateFormatter.string(from: date)
  }
  var formattedEditedDate: String {
    guard let date = edited.toDate else { return edited }
    let dateFormatter = ISO8601DateFormatter()
    dateFormatter.formatOptions = .withFullDate
    return dateFormatter.string(from: date)
  }
}

enum StarshipAction: Equatable {
  case openPilot(URL)
  case openFilm(URL)
  case fetchedPilot(Person)
  case fetchedFilm(Film)
  case fetchingDidFail
  case onSelect
}

struct StarshipEnvironment {
  let fetcher: FetcherServiceProtocol
  let scheduler: AnySchedulerOf<DispatchQueue>
}

let starshipReducer = Reducer<StarshipState, StarshipAction, StarshipEnvironment> { state, action, environment in
  switch action {
  case .openPilot(let url):
    fatalError()
    
  case .openFilm(let url):
    return environment.fetcher
      .fetchFilm(url: url)
      .map { StarshipAction.fetchedFilm($0) }
      .replaceError(with: StarshipAction.fetchingDidFail)
      .eraseToAnyPublisher()
      .receive(on: environment.scheduler)
      .eraseToEffect()

  case .fetchedPilot, .fetchedFilm, .fetchingDidFail, .onSelect:
    // Do nothing. Should be handled in starshipsReducer
    return .none
  }
}
