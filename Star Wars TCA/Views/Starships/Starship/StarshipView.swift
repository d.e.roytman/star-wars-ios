import SwiftUI
import ComposableArchitecture

struct StarshipView: View {
  let store: Store<StarshipState, StarshipAction>
  
  var body: some View {
    WithViewStore(store) { viewStore in
      Form {
        Section("General info") {
          RowView(title: .name, value: viewStore.name)
          RowView(title: .model, value: viewStore.model)
          RowView(title: .manufacturer, value: viewStore.manufacturer)
          RowView(title: .costInCredits, value: viewStore.costInCredits)
          RowView(title: .length, value: viewStore.length)
          RowView(title: .maxAtmospheringSpeed, value: viewStore.maxAtmospheringSpeed)
          RowView(title: .crew, value: viewStore.crew)
          RowView(title: .passengers, value: viewStore.passengers)
          RowView(title: .cargoCapacity, value: viewStore.cargoCapacity)
          RowView(title: .consumables, value: viewStore.consumables)
//          RowView(title: .hyperdriveRating, value: viewStore.hyperdriveRating)
//            RowView(title: .mglt, value: viewStore.mglt)
//            RowView(title: .starshipClass, value: viewStore.starshipClass)
        }
        if !viewStore.pilots.isEmpty {
          Section(String.pilots) {
            ForEach(viewStore.pilots, id: \.self) { pilot in
              Text(pilot.absoluteString)
            }
          }
        }
        if !viewStore.films.isEmpty {
          Section(String.films) {
            ForEach(viewStore.films, id: \.self) { film in
              Text(film.absoluteString)
            }
          }
        }
        Section("Meta info") {
          RowView(title: .created, value: viewStore.formattedCreatedDate)
          RowView(title: .edited, value: viewStore.formattedEditedDate)
        }
      }
    }
  }
}

struct StarshipView_Previews: PreviewProvider {
  static var previews: some View {
    StarshipView(
      store: Store(
        initialState: .preview,
        reducer: starshipReducer,
        environment: .init(
          fetcher: FetcherService(),
          scheduler: DispatchQueue.main.eraseToAnyScheduler()
        )
      )
    )
  }
}

extension StarshipState {
  static var preview: Self {
    return .init(
      name: "CR90 corvette",
      model: "CR90 corvette",
      manufacturer: "Corellian Engineering Corporation",
      costInCredits: "3500000",
      length: "150",
      maxAtmospheringSpeed: "950",
      crew: "30-165",
      passengers: "600",
      cargoCapacity: "3000000",
      consumables: "1 year",
      hyperdriveRating: "2.0",
      mglt: "60",
      starshipClass: "corvette",
      pilots: [],
      films:  [
        URL(string: "https://swapi.dev/api/films/1/")!,
        URL(string: "https://swapi.dev/api/films/3/")!,
        URL(string: "https://swapi.dev/api/films/6/")!
      ],
      created: "2014-12-10T14:20:33.369000Z",
      edited: "2014-12-20T21:23:49.867000Z",
      url: URL(string: "https://swapi.dev/api/starships/2/")!
    )
  }
}
