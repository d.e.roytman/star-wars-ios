import Foundation

extension String {
  static var name: String { "Name" }
  static var model: String { "Model" }
  static var manufacturer: String { "Manufacturer" }
  static var costInCredits: String { "Cost in Credits" }
  static var length: String { "Length" }
  static var maxAtmospheringSpeed: String { "Max Atmosphering speed" }
  static var crew: String { "Crew" }
  static var passengers: String { "Passengers" }
  static var cargoCapacity: String { "Cargo capacity" }
  static var consumables: String { "Consumables" }
  static var hyperdriveRating: String { "Hyperdrive Rating" }
  static var mglt: String { "MGLT" }
  static var starshipClass: String { "Starship class" }
  static var pilots: String { "Pilots" }
  static var films: String { "Films" }
  static var created: String { "Created" }
  static var edited: String { "Edited" }
  static var url: String { "Original JSON" }
}
