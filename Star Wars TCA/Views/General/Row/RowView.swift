import SwiftUI

struct RowView: View {
  let title: String
  let value: String
  
  var body: some View {
    HStack {
      Text(title)
      Spacer()
      Text(value)
    }
  }
}

struct RowView_Previews: PreviewProvider {
  static var previews: some View {
    RowView(title: "name", value: "CR90 Corvette")
  }
}
