import SwiftUI
import ComposableArchitecture

struct BriefState: Equatable {
  typealias Kind = Category.Kind
  let type: Kind
  let text: String
  let url: URL
}

enum BriefAction: Equatable {
  typealias Kind = BriefState.Kind
  case onSelect
  case userDidSelect(Kind, URL)
}

typealias BriefReducer = Reducer<BriefState, BriefAction, Void>
let briefReducer = BriefReducer { state, action, environment in
  switch action {
  case .onSelect:
    return Effect(value: BriefAction.userDidSelect(state.type, state.url))
  case .userDidSelect:
    return .none
  }
}

struct BriefView: View {
  let store: Store<BriefState, BriefAction>
  var body: some View {
    WithViewStore(store) { viewStore in
      Button {
        viewStore.send(.onSelect)
      } label: {
        HStack { Text(viewStore.text) }
      }
    }
  }
}

struct BriefView_Previews: PreviewProvider {
  static var previews: some View {
    BriefView(
      store: .init(
        initialState: BriefState(
          type: .starships,
          text: "CR90 Corvette",
          url: URL(string: "https://swapi.dev/api/starships/2/")!
        ),
        reducer: .empty,
        environment: ()
      )
    )
  }
}
