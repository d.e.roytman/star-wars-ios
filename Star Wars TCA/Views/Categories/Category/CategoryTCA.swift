import ComposableArchitecture
import UIKit

struct CategoryState: Equatable, Identifiable {
  var title: String { return kind.rawValue.capitalized }
  var iconName: String { return kind.iconName }
  var kind: Category.Kind
  var id = UUID()
  var url: URL
}

enum CategoryAction: Equatable {
  case onButtonTap
  case fetchingDidFail(Category.Kind)
  case filmsDidFetch(Films)
  case peopleDidFetch(People)
  case starshipsDidFetch(Starships)
  case vehiclesDidFetch(Vehicles)
  case planetsDidFetch(Planets)
  case speciesDidFetch(Species)
}

struct CategoryEnvironment {
  let scheduler: AnySchedulerOf<DispatchQueue>
  let fetcherService: FetcherServiceProtocol
}

let categoryReducer = Reducer<CategoryState, CategoryAction, CategoryEnvironment> { state, action, environment in
  switch action {
  case .onButtonTap:
    switch state.kind {
    case .people:
      return environment.fetcherService
        .fetchPeople()
        .map { CategoryAction.peopleDidFetch($0) }
        .replaceError(with: CategoryAction.fetchingDidFail(state.kind))
        .eraseToAnyPublisher()
        .receive(on: environment.scheduler)
        .eraseToEffect()

    case .planets:
      return environment.fetcherService
        .fetchPlanets()
        .map { CategoryAction.planetsDidFetch($0) }
        .replaceError(with: CategoryAction.fetchingDidFail(state.kind))
        .eraseToAnyPublisher()
        .receive(on: environment.scheduler)
        .eraseToEffect()
      
    case .films:
      return environment.fetcherService
        .fetchFilms()
        .map { CategoryAction.filmsDidFetch($0) }
        .replaceError(with: CategoryAction.fetchingDidFail(state.kind))
        .eraseToAnyPublisher()
        .receive(on: environment.scheduler)
        .eraseToEffect()
      
    case .species:
      return environment.fetcherService
        .fetchSpecies()
        .map { CategoryAction.speciesDidFetch($0) }
        .replaceError(with: CategoryAction.fetchingDidFail(state.kind))
        .eraseToAnyPublisher()
        .receive(on: environment.scheduler)
        .eraseToEffect()
      
    case .vehicles:
      return environment.fetcherService
        .fetchVehicles()
        .map { CategoryAction.vehiclesDidFetch($0) }
        .replaceError(with: CategoryAction.fetchingDidFail(state.kind))
        .eraseToAnyPublisher()
        .receive(on: environment.scheduler)
        .eraseToEffect()
      
    case .starships:
      return environment.fetcherService
        .fetchStarships()
        .map { CategoryAction.starshipsDidFetch($0) }
        .replaceError(with: CategoryAction.fetchingDidFail(state.kind))
        .eraseToAnyPublisher()
        .receive(on: environment.scheduler)
        .eraseToEffect()
    }

  case .fetchingDidFail, .filmsDidFetch, .peopleDidFetch, .starshipsDidFetch, .vehiclesDidFetch, .planetsDidFetch, .speciesDidFetch:
    return .none
  }
}

extension Store {
  static func makeCategory(with kind: Category.Kind) -> Store<CategoryState, CategoryAction> {
    return Store<CategoryState, CategoryAction>(
      initialState: CategoryState(kind: kind, url: .people),
      reducer: categoryReducer,
      environment: .init(
        scheduler: DispatchQueue.main.eraseToAnyScheduler(),
        fetcherService: FetcherService()
      )
    )
  }
}

extension Category.Kind {
  var iconName: String {
    switch self {
    case .people:
      return "person.3.fill"
    case .planets:
      return "sparkle"
    case .films:
      return "tv.and.mediabox"
    case .species:
      return "person"
    case .vehicles:
      return "car.2"
    case .starships:
      return "train.side.front.car"
    }
  }
}
