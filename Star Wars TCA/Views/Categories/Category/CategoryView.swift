import SwiftUI
import ComposableArchitecture

struct CategoryView: View {
  let store: Store<CategoryState, CategoryAction>
  
  var body: some View {
    WithViewStore(store) { viewStore in
      Button {
        viewStore.send(.onButtonTap)
      } label: {
        HStack {
          Text(viewStore.title)
            .padding(.leading, .paddingS)
          Spacer()
          HStack {
            Image(systemName: viewStore.iconName)
              .padding(.trailing, .paddingS)
            Image(systemName: "arrow.forward.circle.fill")
          }
          .padding(.horizontal, .paddingS)
        }
        .padding(.paddingM)
      }
    }
  }
}

struct CategoryView_Previews: PreviewProvider {
    static var previews: some View {
      Group {
        ForEach(Category.Kind.allCases, id: \.rawValue) { kind in
          CategoryView(store: .makeCategory(with: kind))
        }
      }
    }
}
