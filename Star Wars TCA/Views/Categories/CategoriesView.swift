import ComposableArchitecture
import SwiftUI

struct CategoriesView: View {
  let store: Store<CategoriesState, CategoriesAction>
  var body: some View {
    WithViewStore(store) { viewStore in
      ScrollView {
        LazyVStack {
          ForEachStore(
            store.scope(
              state: \.categories,
              action: CategoriesAction.category(id:action:)
            ),
            content: CategoryView.init(store:)
          )
          Spacer()
        }.onAppear {
          viewStore.send(.onAppear)
        }
      }
    }
  }
}

struct CategoriesView_Previews: PreviewProvider {
  static var previews: some View {
    CategoriesView(store: .categoriesStore)
  }
}
