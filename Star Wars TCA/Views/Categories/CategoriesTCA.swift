import Foundation
import ComposableArchitecture
import SwiftUI

struct CategoriesState: Equatable {
  var categories: IdentifiedArrayOf<CategoryState> = []
}

enum CategoriesAction: Equatable {
  case onAppear
  case category(id: CategoryState.ID, action: CategoryAction)
  case fetchingDidFail
  case categoriesDidFetch([Category])
}

struct CategoriesEnvironment {
  let scheduler: AnySchedulerOf<DispatchQueue>
  let fetcherService: FetcherServiceProtocol
}

typealias CategoriesReducer = Reducer<CategoriesState, CategoriesAction, CategoriesEnvironment>
let categoriesReducer = CategoriesReducer.combine(
  categoryReducer.forEach(
    state: \CategoriesState.categories,
    action: /CategoriesAction.category(id:action:),
    environment: { env in
      CategoryEnvironment(scheduler: env.scheduler, fetcherService: env.fetcherService)
    }
  ),
  Reducer { state, action, environment in
    switch action {
    case .fetchingDidFail, .category:
      // Do nothing
      return .none
      
    case .onAppear:
      return environment.fetcherService
        .fetchCategories()
        .map(\.categories)
        .map { CategoriesAction.categoriesDidFetch($0) }
        .replaceError(with: CategoriesAction.fetchingDidFail)
        .eraseToAnyPublisher()
        .receive(on: environment.scheduler)
        .eraseToEffect()
      
    case .categoriesDidFetch(let categories):
      let mapped = categories
        .map { category in
          return CategoryState(kind: category.kind, url: category.url)
        }
      state.categories = IdentifiedArrayOf(uniqueElements: mapped)
      return .none
    }
  }
)
#if DEBUG
  .debug()
#endif

// MARK: - Helper

extension Store where State == CategoriesState, Action == CategoriesAction {
  static var categoriesStore: Store {
    return Store(
      initialState: .init(),
      reducer: categoriesReducer,
      environment: .default
    )
  }
}

extension CategoriesEnvironment {
  static var `default`: CategoriesEnvironment {
    return .init(
      scheduler: DispatchQueue.main.eraseToAnyScheduler(),
      fetcherService: FetcherService()
    )
  }
}
