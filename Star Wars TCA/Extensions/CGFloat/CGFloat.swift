import UIKit

extension CGFloat {
  static var paddingL: CGFloat { 16 }
  static var paddingM: CGFloat { 12 }
  static var paddingS: CGFloat { 8 }
  static var paddingXS: CGFloat { 4 }
}
