import Foundation

extension String {
  var toDate: Date? {
    let formatter: DateFormatter = .iso8601
    return formatter.date(from: self)
  }
}
