import Foundation

extension String {
  var toURL: URL? { return URL(string: self) }
}

extension Array where Element == String {
  var toURLs: [URL] { compactMap { $0.toURL } }
}
